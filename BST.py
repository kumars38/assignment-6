class Node:
    def __init__(self, key):
        self.left = None
        self.right = None
        self.key = key

class BST:
    def __init__(self, root=None):
        self.root = root

    def insert(self, key):
        if self.root is None:
            self.root = Node(key)
        else:
            self.utility_insert(self.root, key)

    def utility_insert(self, this_node, key):
        if this_node.key > key:
            if this_node.left is None:
                this_node.left = Node(key)
            else:
                self.utility_insert(this_node.left, key)
        else:
            if this_node.right is None:
                this_node.right = Node(key)
            else:
                self.utility_insert(this_node.right, key)
    
    def get_height(self, root):
        if root is None:
            return 0;

        return max(self.get_height(root.left), self.get_height(root.right)) + 1;


bst = BST()
bst.insert(3)
bst.insert(1)
bst.insert(5)
bst.insert(2)

print(bst.get_height(bst.root))