class BSTNode:

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        self.parent = None

    def is_leaf(self):
        return self.left == None and self.right == None

    def is_left_child(self):
        return self == self.parent.left

    def is_right_child(self):
        return not self.is_left_child()

    def __str__(self):
        return "(" + str(self.value) + ")"

    def __repr__(self):
         return "(" + str(self.value) + ")"

    def rotate_right(self):
        # Initial pointers
        l = self.left
        p = self.parent
        newRoot = None

        # Left subtree must exist for a rotation to occur
        if l != None:
            # If the left child has a right subtree, 
            # assign it as the left child of node
            if l.right != None:
                self.left.right.parent = self
                self.left = self.left.right
            else:
                self.left = None

            # Node is the root, make its original left child the new root
            if p == None:
                l.parent = None
                newRoot = l

            # Else if node is a left child, make its original left child the new left child of parent
            elif self.is_left_child():
                p.left = l
                l.parent = p

            # Else node is a right child, make its original left child the new right child of parent
            else:
                p.right = l
                l.parent = p

            # Make original left child the parent of node
            self.parent = l
            l.right = self
        return newRoot

    def rotate_left(self):
        # Initial pointers
        r = self.right
        p = self.parent
        newRoot = None

        # Right subtree must exist for a rotation to occur
        if r != None:
            # If the right child has a left subtree, 
            # assign it as the right child of node
            if r.left != None:
                self.right.left.parent = self
                self.right = self.right.left
            else:
                self.right = None

            # Node is the root, make its original right child the new root
            if p == None:
                r.parent = None
                newRoot = r

            # Else if node is a left child, make its original right child the new left child of parent
            elif self.is_left_child():
                p.left = r
                r.parent = p
            # Else node is a right child, make its original right child the new right child of parent
            else:
                p.right = r
                r.parent = p

            # Make original right child the parent of node
            self.parent = r
            r.left = self
        return newRoot


class BST:

    def __init__(self):
        self.root = None

    def rotate_left(self, val):
        node = self.root
        while (node.value != val):
            if val > node.value:
                node = node.right
            else:
                node = node.left
        root = node.rotate_left()
        if (root != None):
            self.root = root

    def rotate_right(self, val):
        node = self.root
        while (node.value != val):
            if val > node.value:
                node = node.right
            else:
                node = node.left
        root = node.rotate_right()
        if (root != None):
            self.root = root

    def is_empty(self):
        return self.root == None

    def get_height(self):
        if self.is_empty():
            return 0
        return self.__get_height(self.root)

    def __get_height(self, node):
        if node == None:
            return 0
        return 1 + max(self.__get_height(node.left), self.__get_height(node.right))

    def insert(self, value):
        if self.is_empty():
            self.root = BSTNode(value)
        else:
            self.__insert(self.root, value)

    def __insert(self, node, value):
        if value < node.value:
            if node.left == None:
                node.left = BSTNode(value)
                node.left.parent = node
            else:
                self.__insert(node.left, value)
        else:
            if node.right == None:
                node.right = BSTNode(value)
                node.right.parent = node
            else:
                self.__insert(node.right, value)
        
    def __str__(self):
        if self.is_empty():
            return "[]"
        return "[" + self.__str_helper(self.root) + "]"

    def __str_helper(self, node):
        if node.is_leaf():
            return "[" + str(node) + "]"
        if node.left == None:
            return "[" + str(node) + " -> " + self.__str_helper(node.right) + "]"
        if node.right == None:
            return "[" +  self.__str_helper(node.left) + " <- " + str(node) + "]"
        return "[" + self.__str_helper(node.left) + " <- " + str(node) + " -> " + self.__str_helper(node.right) + "]"

b = BST()
b.insert(10)
b.insert(7)
b.insert(4)
b.insert(6)
b.insert(9)
b.insert(5)
b.insert(1)

print(b)
b.rotate_left(4)

print(b)

