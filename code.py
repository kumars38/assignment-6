from xlsxwriter import Workbook
import math
import timeit
import random 
from rbt import RBTree
from bst import BST 


def create_random_list(n):
    L = []
    for _ in range(n):
        L.append(random.randint(1,n))
    return L

def create_near_sorted_list(n, factor):
    L = create_random_list(n)
    L.sort()
    for _ in range(math.ceil(n*factor)):
        index1 = random.randint(0, n-1)
        index2 = random.randint(0, n-1)
        L[index1], L[index2] = L[index2], L[index1]
    return L

def test1():
    height_diffs = []
    for j in range(100):
        L = create_random_list(10000)
        rbt = RBTree()
        bintree = BST()
        for i in range (10000):
            rbt.insert(L[i])
            bintree.insert(L[i])
        height_diffs.append(bintree.get_height()-rbt.get_height())
    print(sum(height_diffs)/len(height_diffs))


def test2():
    workbook = Workbook('near_sort.xlsx')
    worksheet = workbook.add_worksheet()
    bin_h =[]
    rbt_h =[]
    for i in range(1,11):
        n = i/10
        bsum =0
        rsum=0
        for j in range(100):
            L = create_near_sorted_list(10000,n)
            rbt = RBTree()
            bintree = BST()
            for z in range(10000):
                rbt.insert(L[z])
                bintree.insert(L[z])
            bsum += bintree.get_height()
            rsum += rbt.get_height()
        bin_h.append(bsum/100)
        rbt_h.append(rsum/100)

    no = 1
    for i in range(10):
        worksheet.write(i, 0, no/10)
        worksheet.write(i, 1, bin_h[i])
        worksheet.write(i, 2, rbt_h[i])
        no += 1
    workbook.close() 
     
#test1()
#test2()